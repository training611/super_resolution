import argparse
import timeit

import cv2
import glob
import os
from PIL import Image
import numpy as np
from basicsr.archs.rrdbnet_arch import RRDBNet
from basicsr.utils.download_util import load_file_from_url
from imageio import imread

from realesrgan import RealESRGANer
from realesrgan.archs.srvgg_arch import SRVGGNetCompact


def main():
    """Inference demo for Real-ESRGAN."""

    models=["RealESRGAN_x4plus","RealESRNet_x4plus","RealESRGAN_x4plus_anime_6B","RealESRGAN_x2plus","realesr-animevideov3","realesr-general-x4v3"]
    image_path = "test_images/image8.jpg"
    model_name=models[1]

    model_path="weights/"+model_name+".pth"
    dni_weight = None
    start = timeit.default_timer()

    if model_name == 'RealESRGAN_x4plus':  # x4 RRDBNet model
        model = RRDBNet(num_in_ch=3, num_out_ch=3, num_feat=64, num_block=23, num_grow_ch=32, scale=4)
        netscale = 4
    elif model_name == 'RealESRNet_x4plus':  # x4 RRDBNet model
        model = RRDBNet(num_in_ch=3, num_out_ch=3, num_feat=64, num_block=23, num_grow_ch=32, scale=4)
        netscale = 4
    elif model_name == 'RealESRGAN_x4plus_anime_6B':  # x4 RRDBNet model with 6 blocks
        model = RRDBNet(num_in_ch=3, num_out_ch=3, num_feat=64, num_block=6, num_grow_ch=32, scale=4)
        netscale = 4
    elif model_name == 'RealESRGAN_x2plus':  # x2 RRDBNet model
        model = RRDBNet(num_in_ch=3, num_out_ch=3, num_feat=64, num_block=23, num_grow_ch=32, scale=2)
        netscale = 2

    elif model_name == 'realesr-animevideov3':  # x4 VGG-style model (XS size)
        model = SRVGGNetCompact(num_in_ch=3, num_out_ch=3, num_feat=64, num_conv=16, upscale=4, act_type='prelu')
        netscale = 4
    elif model_name == 'realesr-general-x4v3':  # x4 VGG-style model (S size)
        model = SRVGGNetCompact(num_in_ch=3, num_out_ch=3, num_feat=64, num_conv=32, upscale=4, act_type='prelu')
        netscale = 4

    print("loading model:",model_name)
    # restorer
    upsampler = RealESRGANer(
        scale=netscale,
        model_path=model_path,
        dni_weight=dni_weight,
        model=model,
        tile=0,
        tile_pad=10,
        pre_pad=0,
        half=not "fp32",
        gpu_id=None)

    os.makedirs("results/"+model_name+"_result", exist_ok=True)

    imgname, extension = os.path.splitext(os.path.basename(image_path))
    img = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
    print('Origin_width: ', img.shape[1])
    print('Origin_height:', img.shape[0])
    print("testing",imgname)

    if len(img.shape) == 3 and img.shape[2] == 4:
        img_mode = 'RGBA'
    else:
        img_mode = None

    output, _ = upsampler.enhance(img, outscale=4)
    if img_mode == 'RGBA':  # RGBA images should be saved in png format
        extension = 'png'

    save_path = os.path.join("results/"+model_name+"_result", f'{imgname}_out{extension}')
    cv2.imwrite(save_path, output)
    stop = timeit.default_timer()
    print('Result_width: ', output.shape[1])
    print('Result_height:', output.shape[0])
    # save comparison image
    os.makedirs("results/"+model_name + "_result/compare", exist_ok=True)
    image = Image.open(image_path)
    cmp_img = np.concatenate((image.resize((img.shape[1]*4, img.shape[0]*4)), output), axis=1)
    cv2.imwrite("results/"+model_name+"_result/compare/"+f'{imgname}_compare.png',cmp_img)
    print('Inference speed: ', (stop - start))

if __name__ == '__main__':
    start = timeit.default_timer()
    main()
    stop = timeit.default_timer()
    print('Time elapsed for each picture: ', (stop - start))

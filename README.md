# Image Super-Resolution

This is the development documentation for Image Super-Resolution.

# Overview
The project enhance the resolution of an image from low-resolution (LR) to high (HR).

Development documentation : https://docs.google.com/document/d/1izX48gKPWYKF2Z8Z_GJEavvI5Ak29H66/edit#

## Environment

The base environment of the system is made using conda.

**Tested system environments**
- Python 3.9.13
- Torch  1.12.1
- Torchvision 0.13.1
- GPU ( Geforce GTX 1660Ti 6GB )
- Ubuntu 20.04

# How to Build
- Please install and setup anaconda on your system.
- Please setup the environment using :
``` 
- pip install realesrgan
- pip install basicsr 
- pip install facexlib 
- pip install gfpgan
- pip install -r requirements.txt
```
# Pretrained weights for testing
- Please first download the necessary pretrained weights for the system
    - **RealESRGAN_x4plus model** 
        - https://github.com/xinntao/Real-ESRGAN/releases/download/v0.1.0/RealESRGAN_x4plus.pth
    - **RealESRNet_x4plus model**
        - https://github.com/xinntao/Real-ESRGAN/releases/download/v0.1.1/RealESRNet_x4plus.pth
    - **RealESRGAN_x4plus_anime_6B model**
        - https://github.com/xinntao/Real-ESRGAN/releases/download/v0.2.2.4/RealESRGAN_x4plus_anime_6B.pth
    - **RealESRGAN_x2plus model**
        - https://github.com/xinntao/Real-ESRGAN/releases/download/v0.2.1/RealESRGAN_x2plus.pth
    - **realesr-animevideov3 model**
        - https://github.com/xinntao/Real-ESRGAN/releases/download/v0.2.5.0/realesr-animevideov3.pth     



**Run the scripts**
```
python3 inference1.py

python3 inference2.py
```
Both scripts give the same results but models names are stored in array inference2.py script. 

Reference repo:https://github.com/xinntao/Real-ESRGAN







